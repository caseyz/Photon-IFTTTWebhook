int lux;
String strLux;
int debug = 1;
int interval = 1000;
int led = 7;
int readPin = A0;
int last = 0;

void setup()
  {
    //Declare the cloud variable  
    Particle.variable("lux", lux);
    
    //Setup the LED
    pinMode(led, OUTPUT);

    //Setup the analog pin
    pinMode(readPin, INPUT);
    
  }

  void loop ()
    {
        //Update the variables value
        lux = analogRead(readPin);
        Particle.publish("lux", String(lux), 60, PRIVATE);

        flash();
        if (lux < 1100)
            {
                if (millis() > last + 10000)
                    {
                        webHook(lux);
                    }
            }
    }

void flash()
    {
        digitalWrite(led, HIGH);
        delay(interval);
        digitalWrite(led, LOW);
        delay(interval);
    }   
    
void webHook(int x)
    {
        String str;
        str = String(x);
        
        // Trigger the webhook
        Particle.publish("light", str, 60, PRIVATE);
        last = millis();
    }
